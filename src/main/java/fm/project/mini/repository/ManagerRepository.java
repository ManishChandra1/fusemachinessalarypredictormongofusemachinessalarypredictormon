package fm.project.mini.repository;

import java.math.BigInteger;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fm.project.mini.Manager;

@Repository
public interface ManagerRepository extends MongoRepository<Manager, String> {
	
}
