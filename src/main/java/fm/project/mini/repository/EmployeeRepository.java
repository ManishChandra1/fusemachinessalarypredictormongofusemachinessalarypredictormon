package fm.project.mini.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fm.project.mini.Employee;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {
	public List<Employee> findByManagerId(String managerId);
}
