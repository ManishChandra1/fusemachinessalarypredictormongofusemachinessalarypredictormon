package fm.project.mini.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fm.project.mini.Manager;
import fm.project.mini.repository.ManagerRepository;

@Service
public class ManagerService {

	@Autowired
	private ManagerRepository managerRepository;
	
	public void addManager(Manager manager) {
		managerRepository.save(manager);
	}
	
	public List<Manager> getAllManagers() {
		List<Manager> managers = new ArrayList<>();
		this.managerRepository.findAll().forEach(managers::add);
		return managers;
	}
	
	public Manager getManager(String id) {
		return managerRepository.findOne(id);
	}

	public void updateManager(String id, Manager manager) {
		managerRepository.save(manager);
	}

	public void deleteManager(String id) {
		managerRepository.delete(id);
	}

	public boolean doesManagerExist(Manager manager) {
		Manager mng = managerRepository.findOne(manager.getId());
		if(mng != null) {
			return true;
		}
		return false;
	}
	
	
}
