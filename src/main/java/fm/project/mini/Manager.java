package fm.project.mini;

import java.math.BigInteger;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Manager {

	@Id
	private String id;
	
	@NotNull
	private String name;
	
	@Size(max = 50)
	private String department;
	
	@Size(max = 20)
	private String shift;
	
	@Min(0)
	private int salary;
	
	@Min(0)
	private int noOfProjectsHandled;
	
	protected Manager() {
		
	}
	
	public Manager(String name, String department, String shift, int salary, int noOfProjectsHandled) {
		this.name = name;
		this.department = department;
		this.shift = shift;
		this.salary = salary;
		this.noOfProjectsHandled = noOfProjectsHandled;
	}
	
	public Manager(String id, String name, String department, String shift, int salary, int noOfProjectsHandled) {
		this.name = name;
		this.department = department;
		this.shift = shift;
		this.salary = salary;
		this.noOfProjectsHandled = noOfProjectsHandled;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getShift() {
		return shift;
	}

	public void setShift(String shift) {
		this.shift = shift;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	public int getNoOfProjectsHandled() {
		return noOfProjectsHandled;
	}

	public void setNoOfProjectsHandled(int noOfProjectsHandled) {
		this.noOfProjectsHandled = noOfProjectsHandled;
	}
	
}
