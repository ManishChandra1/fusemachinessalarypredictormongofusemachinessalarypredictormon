package fm.project.mini;

import java.math.BigInteger;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Employee {

	@Id
	private String id;
	
	@NotNull(message = "Employee Name is compulsory")
	private String name;
	
	@Size(max = 50)
	private String department;
	
	@Size(max = 20)
	private String shift;
	
	@Min(0)
	private int salary;
	
	@Min(0)
	private int noOfProjects;
	
	@Range(min=0, max=100)
	private int supervisorScore;
	
	@Range(min=0, max=100)
	private int attendanceScore;
	
	@DBRef
	private Manager manager;
	
	private int salaryIncrease;

	protected Employee() {
		
	}

	public Employee(String name, String department, String shift, int salary, int noOfProjects, int supervisorScore, int attendanceScore, Manager manager) {
		this.name = name;
		this.department = department;
		this.shift = shift;
		this.salary = salary;
		this.noOfProjects = noOfProjects;
		this.supervisorScore = supervisorScore;
		this.attendanceScore = attendanceScore;
		this.manager = manager;
	}

	public Employee(String id, String name, String department, String shift, int salary, int noOfProjects, int supervisorScore, int attendanceScore, Manager manager) {
		this.id = id;
		this.name = name;
		this.department = department;
		this.shift = shift;
		this.salary = salary;
		this.noOfProjects = noOfProjects;
		this.supervisorScore = supervisorScore;
		this.attendanceScore = attendanceScore;
		this.manager = manager;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getShift() {
		return shift;
	}

	public void setShift(String shift) {
		this.shift = shift;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	public int getNoOfProjects() {
		return noOfProjects;
	}

	public void setNoOfProjects(int noOfProjects) {
		this.noOfProjects = noOfProjects;
	}

	public int getSupervisorScore() {
		return supervisorScore;
	}

	public void setSupervisorScore(int supervisorScore) {
		this.supervisorScore = supervisorScore;
	}

	public int getAttendanceScore() {
		return attendanceScore;
	}

	public void setAttendanceScore(int attendanceScore) {
		this.attendanceScore = attendanceScore;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}
	
	public int getSalaryIncrease() {
		return salaryIncrease;
	}

	public void setSalaryIncrease(int salaryIncrease) {
		this.salaryIncrease = salaryIncrease;
	}

}
