package fm.project.mini.controller;

import java.math.BigInteger;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fm.project.mini.Manager;
import fm.project.mini.service.ManagerService;
import fm.project.mini.util.CustomErrorType;

@RestController
public class ManagerController {
	
	@Autowired
	private ManagerService managerService;
	
	@RequestMapping(method=RequestMethod.POST, value="/managers")
	public ResponseEntity<?> addManager(@Valid @RequestBody Manager manager, UriComponentsBuilder ucBuilder) {
		if(manager.getId() != null) {
			if (managerService.doesManagerExist(manager)) {
	            return new ResponseEntity(new CustomErrorType("Unable to create. A manager with id " + 
	            manager.getId() + " already exists."),HttpStatus.CONFLICT);
	        }
		}
		managerService.addManager(manager);
		HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/managers/{id}").buildAndExpand(manager.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping("/managers")
	public ResponseEntity<List<Manager>> getAllManagers() {
		List<Manager> managers = managerService.getAllManagers();
		if (managers.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Manager>>(managers, HttpStatus.OK);
	}
	
	@RequestMapping("/managers/{id}")
	public ResponseEntity<?> getManager(@PathVariable("id") String id) {
		Manager mng = managerService.getManager(id);
		if (mng == null) {
            return new ResponseEntity(new CustomErrorType("Manager with id " + id 
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Manager>(mng, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/managers/{id}")
	public ResponseEntity<?> updateManager(@RequestBody Manager manager, @PathVariable String id) {
		Manager currentManager = managerService.getManager(id);
		if (currentManager == null) {
            return new ResponseEntity(new CustomErrorType("Unable to upate. Manager with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
		managerService.updateManager(id, manager);
		return new ResponseEntity<Manager>(manager, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/managers/{id}")
	public ResponseEntity<?> deleteManager(@PathVariable String id) {
		Manager mng = managerService.getManager(id);
		if (mng == null) {
            return new ResponseEntity(new CustomErrorType("Unable to delete. Manager with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
		managerService.deleteManager(id);
		return new ResponseEntity<Manager>(HttpStatus.NO_CONTENT);
	}
}
