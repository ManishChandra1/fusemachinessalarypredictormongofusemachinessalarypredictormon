package fm.project.mini;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fm.project.mini.repository.ManagerRepository;

@RunWith(SpringRunner.class)
@DataMongoTest(excludeAutoConfiguration = EmbeddedMongoAutoConfiguration.class)
public class FusemachinesSalaryPredictorMongoApplicationTests {

	@Autowired
	private ManagerRepository managerRepository;
	
	Manager manager = new Manager();
	
	@Before
	public void beforeTest() {
		manager.setName("name");
		manager.setDepartment("department");
		manager.setShift("shift");
		manager.setSalary(20000);
		manager.setNoOfProjectsHandled(2);
		managerRepository.save(manager);
	}
	
	@Test
	public void contextLoads() throws Exception {
		Manager mng = this.managerRepository.findOne(manager.getId());
		assertThat(mng.getName()).isEqualTo("name");
        assertThat(mng.getDepartment()).isEqualTo("department");
        assertThat(mng.getShift()).isEqualTo("shift");
        assertThat(mng.getSalary()).isEqualTo(20000);
        assertThat(mng.getNoOfProjectsHandled()).isEqualTo(2);
	}
	
	@After
	public void afterTest() {
		this.managerRepository.delete(manager);
	}


}
